import pytest
from calc import Calculator

calc = Calculator()

def test_add():
    assert calc.add(1, 1) == 2

def test_zero_division():
    with pytest.raises(TypeError):
        calc.add(1, 'abc')